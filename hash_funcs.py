import hashlib

def return_blake2b(msg):
    h = hashlib.blake2b()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_blake2s(msg):
    h = hashlib.blake2s()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha1(msg):
    h = hashlib.sha1()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha224(msg):
    h = hashlib.sha224()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha256(msg):
    h = hashlib.sha256()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha384(msg):
    h = hashlib.sha384()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha3_224(msg):
    h = hashlib.sha224()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha3_256(msg):
    h = hashlib.sha3_256()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha3_384(msg):
    h = hashlib.sha3_384()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha3_512(msg):
    h = hashlib.sha512()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_sha512(msg):
    h = hashlib.sha512()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_shake_128(msg):
    h = hashlib.shake_128()
    h.update(msg.encode("ascii"))
    return h.hexdigest()


def return_shake_256(msg):
    h = hashlib.shake_256()
    h.update(msg.encode("ascii"))
    return h.hexdigest()
