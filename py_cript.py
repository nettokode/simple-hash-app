import tkinter
import hashlib
from hash_funcs import *

class Window(tkinter.Tk):
    def __init__(self):
        super().__init__()
        #self.geometry("350x250")
        self.resizable(height=False, width=False)
        self.title("PY_CRYPT")
        self.config_menu()
        self.make_frames()
        self.make_widg()
        self.pack_widgets()
        self.pack_frames()

    def get_selected_crypt(self):
        self.sel_crypt = self.selected_crypt.get()
        if self.sel_crypt == "blake2b":
            self.text_msg_encrypt.insert(tkinter.END, return_blake2b(self.entry_msg.get()))

        elif self.sel_crypt == "blake2s":
            self.text_msg_encrypt.insert(tkinter.END, return_blake2b(self.entry_msg.get()))

        elif self.sel_crypt == "sha1":
            self.text_msg_encrypt.insert(tkinter.END, return_sha1(self.entry_msg.get()))

        elif self.sel_crypt == "sha224":
            self.text_msg_encrypt.insert(tkinter.END, return_sha224(self.entry_msg.get()))

        elif self.sel_crypt == "sha384":
            self.text_msg_encrypt.insert(tkinter.END, return_sha384(self.entry_msg.get()))

        elif self.sel_crypt == "sha256":
            self.text_msg_encrypt.insert(tkinter.END, return_sha256(self.entry_msg.get()))

        elif self.sel_crypt == "sha512":
            self.text_msg_encrypt.insert(tkinter.END, return_sha512(self.entry_msg.get()))

        elif self.sel_crypt == "sha3_384":
            self.text_msg_encrypt.insert(tkinter.END ,return_sha3_384(self.entry_msg.get()))

        elif self.sel_crypt == "sha3_256":
            self.text_msg_encrypt.insert(tkinter.END, return_sha3_256(self.entry_msg.get()))

        elif self.sel_crypt == "sha3_224":
            self.text_msg_encrypt.insert(tkinter.END, return_sha3_224(self.entry_msg.get()))

        elif self.sel_crypt == "sha3_512":
            self.text_msg_encrypt.insert(tkinter.END, return_sha3_512(self.entry_msg.get()))

        elif self.sel_crypt == "shake_128":
            self.text_msg_encrypt.insert(tkinter.END, return_shake_128(self.entry_msg.get()))

        elif self.sel_crypt == "shake_256":
            self.text_msg_encrypt.insert(tkinter.END, return_shake_256(self.entry_msg.get()))

    def make_frames(self):
        self.frame1 = tkinter.Frame(self)
        self.frame2 = tkinter.Frame(self)
        self.frame3 = tkinter.Frame(self)
        self.frame4 = tkinter.Frame(self)
        self.frame5 = tkinter.Frame(self)
        self.frame6 = tkinter.Frame(self)

    def pack_frames(self):
        self.frame1.pack()
        self.frame2.pack()
        self.frame3.pack()
        self.frame4.pack()
        self.frame5.pack()
        self.frame6.pack()

    def config_menu(self):
        self.cripts_list = ["blake2b", "blake2s", "sha1", "sha224", "sha256", "sha384", "sha3_224",
                            "sha3_256", "sha3_384", "sha3_512", "sha512", "shake_128", "shake_256"]
        self.selected_crypt = tkinter.StringVar()
        self.selected_crypt.set(self.cripts_list[0])

    def make_widg(self):
        self.image = tkinter.PhotoImage(file="logo.png")
        self.logo_label = tkinter.Label(self.frame1, image=self.image)
        self.help_label = tkinter.Label(self.frame2, text="INSIRA A MENSSAGEM A SER ENCRIPITADA ABAIXO\nEM SEGUIDA ESCOLHA O TIPO DE CRIPITOGRAFIA")
        self.bt_conf = tkinter.Button(self.frame3, text="ENCRYPT", height=1, width=15, command=self.get_selected_crypt)
        self.entry_msg = tkinter.Entry(self.frame4, width=20)
        self.text_msg_encrypt = tkinter.Text(self.frame5, height=12, width=33)
        self.menu = tkinter.OptionMenu(self.frame6, self.selected_crypt, *self.cripts_list)
        self.menu["height"] = 1
        self.menu["width"] = 5

    def pack_widgets(self):
        self.logo_label.pack()
        self.help_label.pack()
        self.bt_conf.pack()
        self.entry_msg.pack()
        self.text_msg_encrypt.pack()
        self.menu.pack()

app = Window()
app.mainloop()
